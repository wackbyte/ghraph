//! Visitors.

use {
    crate::graph::{Graph, Id, NodeRef, Set},
    alloc::{collections::VecDeque, vec, vec::Vec},
    core::marker::PhantomData,
    ghost_cell::GhostToken,
};

/// A breadth-first search.
#[derive(Clone, Default)]
pub struct Bfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// The stack of nodes to visit.
    pub stack: VecDeque<NodeRef<'graph, 'brand, N, E, I>>,
    /// The set of visited nodes.
    pub visited: Set,
}

impl<'graph, 'brand, N, E, I> Bfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: Id,
{
    /// Create a breadth-first search from a node.
    pub fn new(
        graph: &Graph<'graph, 'brand, N, E, I>,
        root: NodeRef<'graph, 'brand, N, E, I>,
    ) -> Self {
        Self {
            stack: {
                let mut stack = VecDeque::new();
                stack.push_front(root);
                stack
            },
            visited: graph.node_set(),
        }
    }

    /// Create a breadth-first search from a set of nodes.
    pub fn with(
        graph: &Graph<'graph, 'brand, N, E, I>,
        roots: impl IntoIterator<Item = NodeRef<'graph, 'brand, N, E, I>>,
    ) -> Self {
        Self {
            stack: roots.into_iter().collect(),
            visited: graph.node_set(),
        }
    }

    /// Create a breadth-first search with an empty stack.
    pub fn empty(graph: &Graph<'graph, 'brand, N, E, I>) -> Self {
        Self {
            stack: VecDeque::new(),
            visited: graph.node_set(),
        }
    }

    /// Move to a new root node.
    pub fn goto(&mut self, root: NodeRef<'graph, 'brand, N, E, I>) {
        self.stack.clear();
        self.stack.push_front(root);
    }

    /// Advance to the next node.
    pub fn next(&mut self, token: &GhostToken<'brand>) -> Option<NodeRef<'graph, 'brand, N, E, I>> {
        if let Some(node) = self.stack.pop_front() {
            for edge in &node.borrow(token).edges {
                if self.visited.put(edge.borrow(token).id) {
                    self.stack.push_back(edge.borrow(token).target);
                }
            }
            return Some(node);
        }
        None
    }
}

/// A preorder depth-first search.
#[derive(Clone)]
pub struct PreDfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// The stack of nodes to visit.
    pub stack: Vec<NodeRef<'graph, 'brand, N, E, I>>,
    /// The set of visited nodes.
    pub visited: Set,
}

impl<'graph, 'brand, N, E, I> PreDfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: Id,
{
    /// Create a depth-first search.
    pub fn new(
        graph: &Graph<'graph, 'brand, N, E, I>,
        root: NodeRef<'graph, 'brand, N, E, I>,
    ) -> Self {
        Self {
            stack: vec![root],
            visited: graph.node_set(),
        }
    }

    /// Create a depth-first search from a set of nodes.
    pub fn with(
        graph: &Graph<'graph, 'brand, N, E, I>,
        roots: impl IntoIterator<Item = NodeRef<'graph, 'brand, N, E, I>>,
    ) -> Self {
        Self {
            stack: roots.into_iter().collect(),
            visited: graph.node_set(),
        }
    }

    /// Create a depth-first search with an empty stack.
    pub fn empty(graph: &Graph<'graph, 'brand, N, E, I>) -> Self {
        Self {
            stack: Vec::new(),
            visited: graph.node_set(),
        }
    }

    /// Move to a new root node.
    pub fn goto(&mut self, root: NodeRef<'graph, 'brand, N, E, I>) {
        self.stack.clear();
        self.stack.push(root);
    }

    /// Advance to the next node.
    pub fn next(&mut self, token: &GhostToken<'brand>) -> Option<NodeRef<'graph, 'brand, N, E, I>> {
        while let Some(node) = self.stack.pop() {
            if self.visited.put(node.borrow(token).id) {
                for edge in &node.borrow(token).edges {
                    if !self.visited.has(edge.borrow(token).id) {
                        self.stack.push(edge.borrow(token).target);
                    }
                }
                return Some(node);
            }
        }
        None
    }
}

/// A postorder depth-first search.
pub struct PostDfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// The stack of nodes to visit.
    pub stack: Vec<NodeRef<'graph, 'brand, N, E, I>>,
    /// The set of visited nodes.
    pub visited: Set,
    /// The set of finished nodes.
    pub finished: Set,
}

impl<'graph, 'brand, N, E, I> PostDfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: Id,
{
    /// Create a depth-first search.
    pub fn new(
        graph: &Graph<'graph, 'brand, N, E, I>,
        root: NodeRef<'graph, 'brand, N, E, I>,
    ) -> Self {
        Self {
            stack: vec![root],
            visited: graph.node_set(),
            finished: graph.node_set(),
        }
    }

    /// Create a depth-first search from a set of nodes.
    pub fn with(
        graph: &Graph<'graph, 'brand, N, E, I>,
        roots: impl IntoIterator<Item = NodeRef<'graph, 'brand, N, E, I>>,
    ) -> Self {
        Self {
            stack: roots.into_iter().collect(),
            visited: graph.node_set(),
            finished: graph.node_set(),
        }
    }

    /// Create a depth-first search with an empty stack.
    pub fn empty(graph: &Graph<'graph, 'brand, N, E, I>) -> Self {
        Self {
            stack: Vec::new(),
            visited: graph.node_set(),
            finished: graph.node_set(),
        }
    }

    /// Move to a new root node.
    pub fn goto(&mut self, root: NodeRef<'graph, 'brand, N, E, I>) {
        self.stack.clear();
        self.stack.push(root);
    }

    /// Advance to the next node.
    pub fn next(&mut self, token: &GhostToken<'brand>) -> Option<NodeRef<'graph, 'brand, N, E, I>> {
        while let Some(node) = self.stack.pop() {
            if self.visited.put(node.borrow(token).id) {
                for edge in &node.borrow(token).edges {
                    if !self.visited.has(edge.borrow(token).id) {
                        self.stack.push(edge.borrow(token).target);
                    }
                }
            } else {
                self.stack.pop();
                if self.finished.put(node.borrow(token).id) {
                    return Some(node);
                }
            }
        }
        None
    }
}

/// A trait for graph visitors.
pub trait Walk<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// Advance to the next node.
    fn next(&mut self, token: &GhostToken<'brand>) -> Option<NodeRef<'graph, 'brand, N, E, I>>;
}

impl<'graph, 'brand, N, E, I> Walk<'graph, 'brand, N, E, I> for Bfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: Id,
{
    fn next(&mut self, token: &GhostToken<'brand>) -> Option<NodeRef<'graph, 'brand, N, E, I>> {
        self.next(token)
    }
}

impl<'graph, 'brand, N, E, I> Walk<'graph, 'brand, N, E, I> for PreDfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: Id,
{
    fn next(&mut self, token: &GhostToken<'brand>) -> Option<NodeRef<'graph, 'brand, N, E, I>> {
        self.next(token)
    }
}

impl<'graph, 'brand, N, E, I> Walk<'graph, 'brand, N, E, I> for PostDfs<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: Id,
{
    fn next(&mut self, token: &GhostToken<'brand>) -> Option<NodeRef<'graph, 'brand, N, E, I>> {
        self.next(token)
    }
}

/// A walkerator.
pub struct Walker<'walk, 'graph, 'brand, W, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// The token.
    pub token: &'walk GhostToken<'brand>,
    /// The inner walk.
    pub inner: W,
    _marker: PhantomData<NodeRef<'graph, 'brand, N, E, I>>,
}

impl<'walk, 'graph, 'brand, W, N, E, I> Walker<'walk, 'graph, 'brand, W, N, E, I>
where
    W: Walk<'graph, 'brand, N, E, I>,
    N: ?Sized,
    E: ?Sized,
{
    /// Create a walker.
    pub const fn new(token: &'walk GhostToken<'brand>, inner: W) -> Self {
        Self {
            token,
            inner,
            _marker: PhantomData,
        }
    }
}

impl<'walk, 'graph, 'brand, W, N, E, I> Iterator for Walker<'walk, 'graph, 'brand, W, N, E, I>
where
    W: Walk<'graph, 'brand, N, E, I>,
    N: ?Sized,
    E: ?Sized,
{
    type Item = NodeRef<'graph, 'brand, N, E, I>;

    fn next(&mut self) -> Option<Self::Item> {
        self.inner.next(self.token)
    }
}

// SAFETY: `Walker` contains `W` but not `I`, `N`, or `E`. `GhostToken` is `Send` and `Sync`.
unsafe impl<'walk, 'graph, 'brand, W, N, E, I> Send for Walker<'walk, 'graph, 'brand, W, N, E, I>
where
    W: Send,
    N: ?Sized,
    E: ?Sized,
{
}
unsafe impl<'walk, 'graph, 'brand, W, N, E, I> Sync for Walker<'walk, 'graph, 'brand, W, N, E, I>
where
    W: Sync,
    N: ?Sized,
    E: ?Sized,
{
}
