//! Graphviz DOT.

use {
    crate::graph::{EdgeRef, Id, NodeRef},
    alloc::{string::String, vec::Vec},
    core::fmt::{self, Display, Formatter},
    ghost_cell::GhostToken,
};

/// Attributes associated with an object.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Attributes(pub Vec<(String, String)>);

impl Attributes {
    /// Create an empty attribute set.
    pub const fn new() -> Self {
        Self(Vec::new())
    }
}

impl Display for Attributes {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        let mut iter = self.0.iter();
        if let Some((key, value)) = iter.next() {
            write!(f, "{key} = {value}")?;
        }
        for (key, value) in iter {
            write!(f, " {key} = {value}")?;
        }
        Ok(())
    }
}

/// Global formatting options.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Options {
    /// The name of the graph.
    pub name: Option<String>,
    /// Attributes associated with the graph.
    pub graph: Attributes,
    /// Default node attributes.
    pub node: Attributes,
    /// Default edge attributes.
    pub edge: Attributes,
}

/// Graphviz DOT data.
#[derive(Clone, Debug, Default, PartialEq)]
pub struct Dot<I> {
    /// Global formatting options.
    pub options: Options,
    /// Each node's attributes.
    pub nodes: Vec<(I, Attributes)>,
    /// Each edge's attributes.
    pub edges: Vec<(I, I, Attributes)>,
}

impl<I> Dot<I>
where
    I: Copy,
{
    /// Generate the data.
    pub fn build<'graph, 'brand, N, E>(
        token: &GhostToken<'brand>,
        source: impl IntoIterator<Item = NodeRef<'graph, 'brand, N, E, I>>,
        options: Options,
        mut node_attributor: impl FnMut(
            &GhostToken<'brand>,
            NodeRef<'graph, 'brand, N, E, I>,
        ) -> Attributes,
        mut edge_attributor: impl FnMut(
            &GhostToken<'brand>,
            NodeRef<'graph, 'brand, N, E, I>,
            EdgeRef<'graph, 'brand, N, E, I>,
        ) -> Attributes,
    ) -> Self
    where
        'brand: 'graph,
        N: 'graph + ?Sized,
        E: 'graph + ?Sized,
        I: 'graph,
    {
        let mut nodes = Vec::new();
        let mut edges = Vec::new();

        for node in source {
            let node_id = node.borrow(token).id;
            nodes.push((node_id, node_attributor(token, node)));

            for edge in &node.borrow(token).edges {
                let target_id = edge.borrow(token).target.borrow(token).id;
                edges.push((node_id, target_id, edge_attributor(token, node, edge)));
            }
        }

        Self {
            options,
            nodes,
            edges,
        }
    }
}

impl<I> Display for Dot<I>
where
    I: Id,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        // graph
        write!(f, "digraph")?;
        if let Some(name) = &self.options.name {
            write!(f, " {}", name)?;
        }
        writeln!(f, " {{")?;

        if !self.options.graph.0.is_empty() {
            writeln!(f, "    {}", self.options.graph)?;
        }

        if !self.options.node.0.is_empty() {
            writeln!(f, "    node [{}];", self.options.node)?;
        }
        if !self.options.edge.0.is_empty() {
            writeln!(f, "    edge [{}];", self.options.edge)?;
        }

        // nodes
        for (id, node) in &self.nodes {
            if !node.0.is_empty() {
                writeln!(f, "    {} [{}];", id.as_usize(), node)?;
            }
        }

        // edges
        for (from, to, edge) in &self.edges {
            write!(f, "    {} -> {}", from.as_usize(), to.as_usize())?;
            if !edge.0.is_empty() {
                write!(f, " [{}]", edge)?;
            }
            writeln!(f, ";")?;
        }

        write!(f, "}}")?;
        Ok(())
    }
}
