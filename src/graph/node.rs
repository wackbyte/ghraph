//! Nodes.

use {
    super::EdgeRef,
    alloc::vec::Vec,
    ghost_cell::{GhostCell, GhostToken},
};

/// A node.
pub struct Node<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// The identifier.
    pub id: I,
    /// The outgoing edges.
    pub edges: Vec<EdgeRef<'graph, 'brand, N, E, I>>,
    /// The data.
    pub data: N,
}

impl<'graph, 'brand, N, E, I> Node<'graph, 'brand, N, E, I> {
    /// Create a node.
    pub const fn new(id: I, data: N) -> Self {
        Self::new_with_edges(id, Vec::new(), data)
    }

    /// Create a node with a set of edges.
    pub const fn new_with_edges(
        id: I,
        edges: Vec<EdgeRef<'graph, 'brand, N, E, I>>,
        data: N,
    ) -> Self {
        Self { id, edges, data }
    }
}

impl<'graph, 'brand, N, E, I> Node<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// Get the number of outgoing edges the node has.
    pub fn len(&self) -> usize {
        self.edges.len()
    }

    /// Get if the node has no outgoing edges.
    pub fn is_empty(&self) -> bool {
        self.edges.is_empty()
    }

    /// Remove all outgoing edges.
    pub fn clear(&mut self) {
        self.edges.clear();
    }

    /// Add an outgoing edge.
    ///
    /// # Duplicates
    ///
    /// More than one edge to the same node may be added.
    pub fn add(&mut self, edge: EdgeRef<'graph, 'brand, N, E, I>) {
        self.edges.push(edge);
    }

    /// Add a series of outgoing edges.
    ///
    /// # Duplicates
    ///
    /// More than one edge to the same node may be added.
    pub fn extend(&mut self, edges: impl IntoIterator<Item = EdgeRef<'graph, 'brand, N, E, I>>) {
        self.edges.extend(edges);
    }
}

impl<'graph, 'brand, N, E, I> Node<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: PartialEq,
{
    /// Remove an outgoing edge that matches an identifier.
    ///
    /// Returns the edge if it was removed successfully.
    ///
    /// # Duplicates
    ///
    /// In the case of duplicate edges, only the first will be removed.
    pub fn remove(
        &mut self,
        token: &GhostToken<'brand>,
        id: I,
    ) -> Option<EdgeRef<'graph, 'brand, N, E, I>> {
        let mut i = 0;
        while i < self.edges.len() {
            if self.edges[i].borrow(token).id == id {
                return Some(self.edges.remove(i));
            } else {
                i += 1;
            }
        }
        None
    }

    /// Remove an outgoing edge whose target matches an identifier.
    ///
    /// Returns the edge if it was removed successfully.
    ///
    /// # Duplicates
    ///
    /// In the case of duplicate edges, only the first will be removed.
    pub fn remove_by_target(
        &mut self,
        token: &GhostToken<'brand>,
        id: I,
    ) -> Option<EdgeRef<'graph, 'brand, N, E, I>> {
        let mut i = 0;
        while i < self.edges.len() {
            if self.edges[i].borrow(token).target.borrow(token).id == id {
                return Some(self.edges.remove(i));
            } else {
                i += 1;
            }
        }
        None
    }

    /// Remove all outgoing edges that match an identifier.
    ///
    /// Returns the first removed edge if any were removed successfully.
    ///
    /// # Duplicates
    ///
    /// All duplicate edges will be removed.
    pub fn remove_all(
        &mut self,
        token: &GhostToken<'brand>,
        id: I,
    ) -> Option<EdgeRef<'graph, 'brand, N, E, I>> {
        let mut found = None;
        let mut i = 0;
        while i < self.edges.len() {
            if self.edges[i].borrow(token).id == id {
                let node = self.edges.remove(i);
                if found.is_none() {
                    found = Some(node);
                }
            } else {
                i += 1;
            }
        }
        found
    }

    /// Remove all outgoing edges whose target matches an identifier.
    ///
    /// Returns the first removed edge if any were removed successfully.
    ///
    /// # Duplicates
    ///
    /// All duplicate edges will be removed.
    pub fn remove_all_by_target(
        &mut self,
        token: &GhostToken<'brand>,
        id: I,
    ) -> Option<EdgeRef<'graph, 'brand, N, E, I>> {
        let mut found = None;
        let mut i = 0;
        while i < self.edges.len() {
            if self.edges[i].borrow(token).target.borrow(token).id == id {
                let node = self.edges.remove(i);
                if found.is_none() {
                    found = Some(node);
                }
            } else {
                i += 1;
            }
        }
        found
    }
}

/// A node behind a [`GhostCell`].
pub type NodeCell<'graph, 'brand, N, E, I> = GhostCell<'brand, Node<'graph, 'brand, N, E, I>>;

/// An immutable reference to a node behind a [`GhostCell`].
pub type NodeRef<'graph, 'brand, N, E, I> = &'graph NodeCell<'graph, 'brand, N, E, I>;

/// A mutable reference to a node behind a [`GhostCell`].
pub type NodeMut<'graph, 'brand, N, E, I> = &'graph mut NodeCell<'graph, 'brand, N, E, I>;
