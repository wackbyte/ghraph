{
  description = "ghraph";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, flake-utils, fenix, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        lib = pkgs.lib;
        toolchain = fenix.packages.${system}.stable;
      in
      {
        devShell = pkgs.mkShell {
          nativeBuildInputs = [
            toolchain.cargo
            toolchain.clippy
            toolchain.rustc
            toolchain.rustfmt
            pkgs.nixpkgs-fmt
          ];
        };
      }
    );
}
