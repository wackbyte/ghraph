# ghraph

A ghostly directed graph implementation using [`GhostCell`](https://github.com/matthieu-m/ghost-cell).

Currently a work in progress.

## Credits

Inspired by [`petgraph`](https://github.com/petgraph/petgraph) and [the original `GhostCell` DFS implementation](https://gitlab.mpi-sws.org/FP/ghostcell/-/blob/master/ghostcell/src/dfs_arena.rs).
