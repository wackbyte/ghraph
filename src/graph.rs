//! Graphs.

use {alloc::vec::Vec, core::marker::PhantomData, ghost_cell::GhostCell};

pub mod edge;
pub mod id;
pub mod node;

#[doc(inline)]
pub use self::{
    edge::{Edge, EdgeCell, EdgeMut, EdgeRef},
    id::{Id, Set},
    node::{Node, NodeCell, NodeMut, NodeRef},
};

/// A graph.
///
/// # Uniqueness
///
/// Nodes and edges are assigned unique identifiers, but only relative to the graph.
/// It is possible to accidentally share them between graphs.
#[derive(Debug)]
pub struct Graph<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// The current node identifier.
    node_counter: I,
    /// The current edge identifier.
    edge_counter: I,
    _marker: PhantomData<(
        NodeRef<'graph, 'brand, N, E, I>,
        EdgeRef<'graph, 'brand, N, E, I>,
    )>,
}

impl<'graph, 'brand, N, E, I> Graph<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: Id,
{
    /// Create an empty graph.
    pub const fn new() -> Self {
        Self {
            node_counter: I::ZERO,
            edge_counter: I::ZERO,
            _marker: PhantomData,
        }
    }

    /// Advance to the next node identifier.
    fn next_node(&mut self) -> I {
        let id = self.node_counter;
        self.node_counter = self.node_counter.next();
        id
    }

    /// Advance to the next edge identifier.
    fn next_edge(&mut self) -> I {
        let id = self.edge_counter;
        self.edge_counter = self.edge_counter.next();
        id
    }

    /// Get the number of nodes in the graph.
    pub fn node_count(&self) -> usize {
        self.node_counter.as_usize()
    }

    /// Get the number of edges in the graph.
    pub fn edge_count(&self) -> usize {
        self.edge_counter.as_usize()
    }

    /// Get if the graph has no nodes.
    pub fn no_nodes(&self) -> bool {
        self.node_counter == I::ZERO
    }

    /// Get if the graph has no edges.
    pub fn no_edges(&self) -> bool {
        self.edge_counter == I::ZERO
    }

    /// Create a set for marking the graph's nodes.
    pub fn node_set(&self) -> Set {
        Set::nodes(self)
    }

    /// Create a set for marking the graph's edges.
    pub fn edge_set(&self) -> Set {
        Set::edges(self)
    }

    /// Reset a set to fit the graph's nodes.
    pub fn reset_nodes(&self, set: &mut Set) {
        set.reset_nodes(self);
    }

    /// Reset a set to fit the graph's edges.
    pub fn reset_edges(&self, set: &mut Set) {
        set.reset_edges(self);
    }
}

impl<'graph, 'brand, N, E, I> Graph<'graph, 'brand, N, E, I>
where
    I: Id,
{
    /// Add a node to the graph.
    pub fn node(&mut self, data: N) -> NodeCell<'graph, 'brand, N, E, I> {
        let id = self.next_node();
        GhostCell::new(Node::new(id, data))
    }

    /// Add a node with a set of edges to the graph.
    pub fn node_with_edges(
        &mut self,
        edges: Vec<EdgeRef<'graph, 'brand, N, E, I>>,
        data: N,
    ) -> NodeCell<'graph, 'brand, N, E, I> {
        let id = self.next_node();
        GhostCell::new(Node::new_with_edges(id, edges, data))
    }

    /// Add an edge to the graph.
    pub fn edge(
        &mut self,
        target: NodeRef<'graph, 'brand, N, E, I>,
        data: E,
    ) -> EdgeCell<'graph, 'brand, N, E, I> {
        let id = self.next_edge();
        GhostCell::new(Edge::new(id, target, data))
    }
}

impl<'graph, 'brand, N, E, I> Default for Graph<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
    I: Id,
{
    fn default() -> Self {
        Self::new()
    }
}

// SAFETY: `Graph` contains `I` but not `N` or `E`.
unsafe impl<'graph, 'brand, N, E, I> Send for Graph<'graph, 'brand, N, E, I> where I: Send {}
unsafe impl<'graph, 'brand, N, E, I> Sync for Graph<'graph, 'brand, N, E, I> where I: Sync {}
