//! Edges.

use {super::NodeRef, ghost_cell::GhostCell};

/// An edge.
pub struct Edge<'graph, 'brand, N, E, I>
where
    N: ?Sized,
    E: ?Sized,
{
    /// The identifier.
    pub id: I,
    /// The target node.
    pub target: NodeRef<'graph, 'brand, N, E, I>,
    /// The data.
    pub data: E,
}

impl<'graph, 'brand, N, E, I> Edge<'graph, 'brand, N, E, I> {
    /// Create an edge.
    pub const fn new(id: I, target: NodeRef<'graph, 'brand, N, E, I>, data: E) -> Self {
        Self { id, data, target }
    }
}

/// An edge behind a [`GhostCell`].
pub type EdgeCell<'graph, 'brand, N, E, I> = GhostCell<'brand, Edge<'graph, 'brand, N, E, I>>;

/// An immutable reference to an edge behind a [`GhostCell`].
pub type EdgeRef<'graph, 'brand, N, E, I> = &'graph EdgeCell<'graph, 'brand, N, E, I>;

/// A mutable reference to an edge behind a [`GhostCell`].
pub type EdgeMut<'graph, 'brand, N, E, I> = &'graph mut EdgeCell<'graph, 'brand, N, E, I>;
