#![doc = include_str!("../README.md")]
#![cfg_attr(not(any(doc, test)), no_std)]
#![warn(missing_docs)]

extern crate alloc;

pub mod algo;
pub mod dot;
pub mod graph;
pub mod visit;

#[doc(inline)]
pub use self::graph::{
    Edge, EdgeCell, EdgeMut, EdgeRef, Graph, Id, Node, NodeCell, NodeMut, NodeRef, Set,
};
