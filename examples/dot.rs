use {
    ghost_cell::GhostToken,
    ghraph::{
        dot::{Attributes, Dot, Options},
        visit::{PreDfs, Walker},
        Graph,
    },
};

fn main() {
    let _ = GhostToken::new(|mut token| {
        // setup
        let mut graph = Graph::<&str, (), usize>::new();
        let a = graph.node("a");
        let b = graph.node("b");
        let c = graph.node("c");
        let d = graph.node("d");
        let e = graph.node("e");

        let a_to_e = graph.edge(&e, ());
        let a_to_b = graph.edge(&b, ());

        let b_to_e = graph.edge(&e, ());
        let b_to_c = graph.edge(&c, ());

        let c_to_e = graph.edge(&e, ());
        let c_to_d = graph.edge(&d, ());

        let d_to_e = graph.edge(&e, ());
        let d_to_a = graph.edge(&a, ());

        // a  →  b
        //  ↘︎   ↙︎
        // ↑  e  ↓
        //  ↗︎   ↖︎
        // d  ←  c
        a.borrow_mut(&mut token).extend([&a_to_e, &a_to_b]);
        b.borrow_mut(&mut token).extend([&b_to_e, &b_to_c]);
        c.borrow_mut(&mut token).extend([&c_to_e, &c_to_d]);
        d.borrow_mut(&mut token).extend([&d_to_e, &d_to_a]);

        let dfs = Walker::new(&token, PreDfs::new(&graph, &a));
        println!(
            "{}",
            Dot::build(
                &token,
                dfs,
                Options::default(),
                |token, node| Attributes(vec![(
                    "label".to_owned(),
                    format!("\"{}\"", node.borrow(token).data),
                )]),
                |token, node, edge| Attributes(vec![(
                    "label".to_owned(),
                    format!(
                        "\"{} → {}\"",
                        node.borrow(token).data,
                        edge.borrow(token).target.borrow(token).data,
                    ),
                )]),
            ),
        );
    });
}
