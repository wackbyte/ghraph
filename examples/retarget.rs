use {
    ghost_cell::GhostToken,
    ghraph::{
        dot::{Attributes, Dot, Options},
        visit::{PreDfs, Walker},
        Graph,
    },
};

fn main() {
    let _ = GhostToken::new(|mut token| {
        // setup
        let mut graph = Graph::<&str, &str, usize>::new();
        let a = graph.node("a");
        let b = graph.node("b");
        let c = graph.node("c");
        let d = graph.node("d");
        let t1 = graph.node("t1");
        let t2 = graph.node("t2");

        let edge = graph.edge(&t1, "→ t1");

        a.borrow_mut(&mut token).add(&edge);
        b.borrow_mut(&mut token).add(&edge);
        c.borrow_mut(&mut token).add(&edge);
        d.borrow_mut(&mut token).add(&edge);

        //   b    c
        //    ↘︎  ↙︎
        // a → t1 ← d
        println!("# before");
        let dfs = Walker::new(&token, PreDfs::with(&graph, [&a, &b, &c, &d]));
        println!(
            "{}",
            Dot::build(
                &token,
                dfs,
                Options::default(),
                |token, node| Attributes(vec![(
                    "label".to_owned(),
                    format!("\"{}\"", node.borrow(token).data),
                )]),
                |token, _node, edge| Attributes(vec![(
                    "label".to_owned(),
                    format!("\"{}\"", edge.borrow(token).data,),
                )]),
            ),
        );

        // retarget the shared edge
        {
            let edge = edge.borrow_mut(&mut token);
            edge.data = "→ t2";
            edge.target = &t2;
        }

        //   b    c
        //    ↘︎  ↙︎
        // a → t2 ← d
        println!("# after");
        let dfs = Walker::new(&token, PreDfs::with(&graph, [&a, &b, &c, &d]));
        println!(
            "{}",
            Dot::build(
                &token,
                dfs,
                Options::default(),
                |token, node| Attributes(vec![(
                    "label".to_owned(),
                    format!("\"{}\"", node.borrow(token).data),
                )]),
                |token, _node, edge| Attributes(vec![(
                    "label".to_owned(),
                    format!("\"{}\"", edge.borrow(token).data,),
                )]),
            ),
        );
    });
}
