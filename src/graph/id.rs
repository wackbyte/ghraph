//! Identifiers.

use {super::Graph, fixedbitset::FixedBitSet};

/// Monotonically increasing identifiers.
///
/// # Safety
///
/// - [`ZERO`](Self::ZERO) must be zero (or the first/lowest value).
/// - [`next`](Self::next) must be monotonically increasing from [`ZERO`](Self::ZERO).
/// - [`as_usize`](Self::as_usize) must return an accurate value.
pub unsafe trait Id: Copy + Clone + Eq + PartialEq + Ord + PartialOrd {
    /// The first identifier.
    const ZERO: Self;

    /// Get the next identifier.
    fn next(self) -> Self;

    /// Convert the identifier into a `usize`.
    fn as_usize(self) -> usize;
}

macro_rules! impl_id {
    (
        $(
            $T:ty
        ),*
        $(,)?
    ) => {
        $(
            unsafe impl Id for $T {
                const ZERO: Self = 0;

                #[inline(always)]
                fn next(self) -> Self {
                    self + 1
                }

                #[inline(always)]
                fn as_usize(self) -> usize {
                    self as usize
                }
            }
        )*
    };
}

impl_id!(usize, u8, u16, u32, u64, u128);

/// A set of visited identifiers.
#[derive(Clone, Debug, Default, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Set {
    /// The inner bit set.
    inner: FixedBitSet,
}

impl Set {
    /// Create an empty set.
    pub const fn new() -> Self {
        Self {
            inner: FixedBitSet::new(),
        }
    }

    /// Create a set to fit a graph's nodes.
    pub fn nodes<N, E, I>(graph: &Graph<N, E, I>) -> Self
    where
        N: ?Sized,
        E: ?Sized,
        I: Id,
    {
        Self {
            inner: FixedBitSet::with_capacity(graph.node_count()),
        }
    }

    /// Create a set to fit a graph's edges.
    pub fn edges<N, E, I>(graph: &Graph<N, E, I>) -> Self
    where
        N: ?Sized,
        E: ?Sized,
        I: Id,
    {
        Self {
            inner: FixedBitSet::with_capacity(graph.edge_count()),
        }
    }

    /// Reset the set to fit a graph's nodes.
    pub fn reset_nodes<N, E, I>(&mut self, graph: &Graph<N, E, I>)
    where
        N: ?Sized,
        E: ?Sized,
        I: Id,
    {
        self.inner.clear();
        self.inner.grow(graph.node_count());
    }

    /// Reset the set to fit a graph's edges.
    pub fn reset_edges<N, E, I>(&mut self, graph: &Graph<N, E, I>)
    where
        N: ?Sized,
        E: ?Sized,
        I: Id,
    {
        self.inner.clear();
        self.inner.grow(graph.edge_count());
    }

    /// Mark a node identifier as visited.
    ///
    /// Returns `true` if it was visited previously, and `false` otherwise.
    pub fn put<I>(&mut self, id: I) -> bool
    where
        I: Id,
    {
        !self.inner.put(id.as_usize())
    }

    /// Get if a node identifier has been visited.
    pub fn has<I>(&self, id: I) -> bool
    where
        I: Id,
    {
        self.inner.contains(id.as_usize())
    }
}
